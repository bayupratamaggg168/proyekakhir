<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModifyEventTable extends Migration
{
    public function up()
    {
        $data = [
            'content'       => [
                'type'       => 'TEXT',
            ],
        ];

        $this->forge->modifyColumn('events', $data);
    }

    public function down()
    {
    }
}
