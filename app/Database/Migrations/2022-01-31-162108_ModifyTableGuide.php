<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModifyTableGuide extends Migration
{
    public function up()
    {
        $this->forge->dropColumn('guides', 'video');
    }

    public function down()
    {
        $this->forge->addColumn('guides', 'video');
    }
}
