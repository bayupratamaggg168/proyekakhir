<?= $this->extend('admin/admin_layout') ?>


<?= $this->section('style') ?>
<!-- recomendation's style -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url() ?>/main/form.css">
<?= $this->endSection() ?>


<?= $this->section('content') ?>

<section class="data-transaction" id="data-transaction">

    <div class="content">
        <h3>Tabel Data Transaksi</h3>
        <br><br>
        <div class="row">
            <div class="col-6">
                <form action="" method="get">
                    <div class="input-group mb-2 mt-5">
                        <input type="text" class="form-control" placeholder="Cari Data" name="keyword" autocomplete="off">
                        <button class="btn btn-outline-secondary" type="submit">Cari</button>
                    </div>
                </form>
            </div>
        </div>
        <?= view('shared/flash_message') ?>
        <table id="example" class="table table-striped">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Status</th>
                     <th>User</th>
                     <th>Guide</th>
                    <th>Tanggal Sewa</th>
                    <th>Tanggal Selesai</th>
                    <th>Tempat Tujuan</th>
                    <th>Kendaraan</th>
                    <th>Biaya</th>
                    <th>Tipe Pembayaran</th>
                    <th>Lokasi Penjemputan</th>
                    <th>Catatan</th>

                </tr>
            </thead>
            <?php
            $no = 1 + (5 * ($currentPage - 1));
            ?>
            <tbody>
                <?php if (count($transaction) > 0) : ?>

                    <?php foreach ($transaction as $item) : ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $item->status ?></td>
                            <td><?= $item->room()->user()->name ?></td>
                            <td><?= $item->room()->guide()->name ?></td>
                            <td><?= \CodeIgniter\I18n\Time::parse($item->date_start)->toLocalizedString('DD MMMM YYYY') ?></td>
                            <td><?= \CodeIgniter\I18n\Time::parse($item->date_finish)->toLocalizedString('DD MMMM YYYY') ?></td>
                            <td><?= $item->destination ?></td>
                            <td><?= $item->transport ?></td>
                            <td>Rp. <?= $item->price ?></td>
                            <td><?= $item->payment ?></td>
                            <td><?= $item->meetpoint ?></td>
                            <td><?= $item->note ?></td>
                        </tr>
                    <?php endforeach ?>
                    <?= $pager->links('transaction', 'pagination'); ?>
                <?php else : ?>
                    <tr>Data tidak Ditemukan</tr>
                <?php endif; ?>

            </tbody>
        </table>
    </div>

</section>

<?= $this->endSection() ?>

<?= $this->section('script') ?>

<script>

</script>
<?= $this->endSection() ?>