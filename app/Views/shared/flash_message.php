<?php if (!empty(session()->getFlashdata('errors'))) : ?>
    <div class="alert alert-danger" id="flash" role="alert">
        <p><strong>Whoops!</strong> There are some problems with your input.</p>
        <ul>
            <?php foreach (session()->getFlashdata('errors') as $field => $error) : ?>
                <li><?= $error ?></li>
            <?php endforeach ?>
        </ul>
    </div>
<?php endif ?>

<?php if (!empty(session()->getFlashdata('success'))) : ?>
    <div class="alert alert-success" id="flash" id="sukses">
        <?= session()->getFlashdata('success') ?>
    </div>
<?php endif; ?>

<?php if (!empty(session()->getFlashdata('error'))) : ?>
    <div class="alert alert-danger" id="flash">
        <?= session()->getFlashdata('error') ?>
    </div>
<?php endif; ?>
